#include "board.h"

// NO GOOD!
volatile static int k = 8000;
void naive_delay(uint32_t time_delay)
{
  for(uint32_t i = 0; i < time_delay * k; i++);
}

// ...
void _delay_ms(uint32_t time_delay)
{
  delay_us(time_delay * 1000);
}

// good
void DWT_Init(void)
{
  
if (!(CoreDebug->DEMCR & CoreDebug_DEMCR_TRCENA_Msk)) {
        CoreDebug->DEMCR |= CoreDebug_DEMCR_TRCENA_Msk;
        DWT->CYCCNT = 0;
        DWT->CTRL |= DWT_CTRL_CYCCNTENA_Msk;
    }
  
  SCB->SCR |= CoreDebug_DEMCR_TRCENA_Msk;
  DWT->CYCCNT  = 0;
  DWT->CTRL |= DWT_CTRL_CYCCNTENA_Msk; 
}

static inline uint32_t delta(uint32_t t0, uint32_t t1)
{
  return (t1 - t0); // :)
}

void delay_us(uint32_t us)
{
  uint32_t t0 =  DWT->CYCCNT;
  uint32_t us_count_tic =  us * (SystemCoreClock / 1000000);
  while (delta(t0, DWT->CYCCNT) < us_count_tic) ;
}
