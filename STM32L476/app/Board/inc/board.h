// Header: Board header
// File Name: board.h
// Author: Kargapolcev M. E.
// Date: 17.12.2019

#ifndef BOARD
#define BOARD

#include "main.h"

// Delay
void DWT_Init(void);
void delay_us(uint32_t us);

#define delay_ms(x) _delay_ms(x)
void naive_delay(uint32_t time_delay);
void _delay_ms(uint32_t time_delay);

// Battery Charger
//#define batt_chrg  (!((GPIOB->IDR >> 1)&1))

//#define KEY_UP    (!((KEY_UP_GPIO_Port->IDR >> 7)&1))
//#define KEY_DOWN  (!((KEY_UP_GPIO_Port->IDR >> 5)&1))

#define BUTTON  (((GPIOB->IDR >> 13)&1))

//Display pins
#define DP_Reset   (1)
#define DP_Set     (0)
//Display reset pin
#define Display_RESET(x)    ((x)? (GPIOB->BSRR = GPIO_BSRR_BR0)  : (GPIOB->BSRR = GPIO_BSRR_BS0));
//Display D/C
#define Display_DC(x)    ((x)? (GPIOB->BSRR=GPIO_BSRR_BR1)  : (GPIOB->BSRR=GPIO_BSRR_BS1));
//Display SPI_CS
#define Display_SPI_CS(x)    ((x)? (GPIOB->BSRR=GPIO_BSRR_BR2)  : (GPIOB->BSRR=GPIO_BSRR_BS2));
//extern SPI_HandleTypeDef hspi2;

void adc1_start(void);

#endif /* BOARD */
