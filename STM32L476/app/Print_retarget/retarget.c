/*----------------------------------------------------------------------------
 * Name:    Retarget.c
 * Purpose: 'Retarget' layer for target-dependent low level functions
 * Note(s):
 *----------------------------------------------------------------------------
 * This file is part of the �Vision/ARM development tools.
 * This software may only be used under the terms of a valid, current,
 * end user licence from KEIL for a compatible version of KEIL software
 * development tools. Nothing else gives you the right to use this software.
 *
 * This software is supplied "AS IS" without warranties of any kind.
 *
 * Copyright (c) 2009 Keil - An ARM Company. All rights reserved.
 *----------------------------------------------------------------------------*/

#include <stdio.h>
#include <rt_misc.h>
#include "retarget.h"
#include "board.h"
#include <string.h>
#pragma import(__use_no_semihosting_swi)


int pointer = 0;
#define BUF_SZ  100
uint8_t  buf[BUF_SZ];

// Circular update
void printf_fifo_update(void)
{
  int i = 0;
  while(i != pointer)
  {
    LL_USART_TransmitData8(USART2, buf[i++]);
    while (!LL_USART_IsActiveFlag_TXE(USART2)) {;}
    LL_USART_ClearFlag_PE(USART2);
  }
  pointer = 0;
  memset(buf, 0, BUF_SZ);
}

//
extern int getkey   (void);


struct __FILE { int handle; /* Add whatever you need here */ };
FILE __stdout;
FILE __stdin;

int sendchar (int c)
{
  buf[pointer++] = c;
  return(c);
}

int fputc(int c, FILE *f) {
  return (sendchar(c));
}


int fgetc(FILE *f) {
  //return (getkey());
  return 0;
}


int ferror(FILE *f) {
  /* Your implementation of ferror */
  return EOF;
}


void _ttywrch(int c) {
  sendchar(c);
}


void _sys_exit(int return_code) {
label:  goto label;  /* endless loop */
}
