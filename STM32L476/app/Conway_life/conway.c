// Conway life

#include "conway.h"
#include "_conway.h"
#include <string.h>

Cell_neighbours _calc_neighbours(Cell u[W][H], int x, int y)
{
  Cell_neighbours n = Cell_zero_nb;
  
  if(x > W || y > H) return n;
  
  int i, j;

  for (i = x - 1; i <= x + 1; i++)
    for (j = y + 1; j >= y - 1; j--)
    {
      if (i == W || j == H || i == -1 || j == -1 || (i == x && j == y))
      {
        ; 
      }
      else
      {
        n += _get_cell_state(u, i%W, j%H);
      }
    }
  return n;
}

void universe_next(Cell u[W][H])
{
  Cell u2[W][H];
  int i, j;
  Cell_neighbours n;
  
  for (i = 0; i < W; i++)
    for (j = 0; j < H; j++)
    {
      n = _calc_neighbours(u, i, j);
      u[i][j]._cell.n = n;
      //_set_cell_neighbours(u, i, j, n);
      
      if ((int)_get_cell_state(u, i, j))    //cell is alive
      {
        if (n < Cell_two_nb || n > Cell_three_nb) //rule number 1 & number 3

          _set_cell_state(u2, i, j, Cell_dead);
        else if (n == Cell_two_nb || n == Cell_three_nb) //rule number 2
        {

          _set_cell_state(u2, i, j, Cell_alive);
        }
        else
          _set_cell_state(u2, i, j, _get_cell_state(u, i, j));
      }
      else
      {
        if (n == Cell_three_nb ) //rule number 4


          _set_cell_state(u2, i, j, Cell_alive);
        else

        _set_cell_state(u2, i, j, _get_cell_state(u, i, j));
      }
    }
  memcpy(u, u2, W*H);
}
