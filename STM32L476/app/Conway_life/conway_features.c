#include "conway.h"
#include "_conway.h"

void _set_cell_neighbours
  (Cell u[W][H], int x, int y, Cell_neighbours n)
{
  (u[x%W][y%H])._cell.n = n;
}

Cell_neighbours _get_cell_neighbours
(Cell u[W][H], int x, int y)
{
  //if((x > H) || (y > W)) return (Cell_neighbours)0;
  return  (u[x%W][y%H])._cell.n;
}
