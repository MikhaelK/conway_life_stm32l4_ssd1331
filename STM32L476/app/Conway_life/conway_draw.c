// Conway life RGB OLED SSD1331 draw
#include "conway.h"
#include "_conway.h"

volatile int disp_gain = 64; //= 0.5f;
volatile int disp_color_table = 0U;
volatile int disp_body_en = 1U;
volatile int disp_shadow_en = 0U;
volatile int disp_offset_x = 0;
volatile int disp_offset_y = 0;
volatile int universe_new_rand = 1U;
volatile int universe_new_clear = 0U;
volatile int pause = 0, step = 0;
// Objects
volatile int universe_add_object0 = 0U;
volatile int universe_add_object1 = 0U;
volatile int glider_flag = 0U;
volatile int new_year = 0U;
volatile int my_osc_flag = 0;
volatile static /*const*/ Color cell_color_table[8][8] = {
  // BW table 5
  (Color)RGB(0, 0, 0),
  (Color)RGB(32, 32, 32),
  (Color)RGB(64, 64, 64),
  (Color)RGB(96, 96, 96),
  (Color)RGB(128, 128, 128),
  (Color)RGB(160, 160, 160),
  (Color)RGB(192, 192, 192),
  (Color)RGB(255, 255, 255),
  // color 6
  (Color)RGB(0, 0, 0),
  (Color)RGB(32, 32, 32),
  (Color)RGB(32, 96, 64),
  (Color)RGB(32, 96, 96),
  (Color)RGB(28, 96, 96),
  (Color)RGB(160, 128, 128),
  (Color)RGB(192, 160, 160),
  (Color)RGB(255, 192, 192),
   
  // color table 7
  (Color)RGB(0, 0, 0),
  (Color)RGB(192, 32, 0),
  (Color)RGB(160, 96, 0),
  (Color)RGB(128, 96, 0),
  (Color)RGB(96, 96, 0),
  (Color)RGB(0, 128, 0),
  (Color)RGB(0, 160, 0),
  (Color)RGB(0, 192, 0),
  
  // color table 8
  (Color)RGB(1, 0, 1),
  (Color)RGB(32, 0, 32),
  (Color)RGB(42, 0, 42),
  (Color)RGB(73, 0, 96),
  (Color)RGB(96, 0, 73),
  (Color)RGB(130, 0, 42),
  (Color)RGB(192, 0, 32),
  (Color)RGB(255, 0, 1),
  
  // color table 9
  (Color)RGB(0, 0, 0),
  (Color)RGB(32, 0, 0),
  (Color)RGB(42, 0, 0),
  (Color)RGB(73, 0, 0),
  (Color)RGB(96, 0, 0),
  (Color)RGB(130, 0, 0),
  (Color)RGB(192, 0, 0),
  (Color)RGB(255, 0, 0),
};

void _draw_dot(int h, int w, int ink)
{
  SSD1331_drawPixel(h, w, cell_color_table[disp_color_table][ink]);
}

void universe_draw_window(Cell u[W][H])
{
  int i= 0 , j = 0; //i -> row j -> column
  int ink = 0;
  int _x = 0, _y = 0;
  //Cell_state st;
  
  if(universe_new_rand != 0) 
  {
    universe_rand(u);
    universe_new_rand = 0;
    printf("universe random\r\n");
  }
  
  if(universe_new_clear != 0) 
  {
    Cell t;
    universe_fill(u, t);
    universe_new_clear = 0;
    printf("universe new clear\r\n");
  }
  
  if(universe_add_object0 != 0) 
  {
    universe_add_nxn(u, disp_offset_x, disp_offset_y, object0);
    universe_add_object0 = 0;
    printf("universe add object\r\n");
  }
  
  if(universe_add_object1 != 0) 
  {
    universe_add_nxn(u, disp_offset_x, disp_offset_y, object0);
    universe_add_object1 = 0;
    printf("universe add object\r\n");
  }
  
  if(glider_flag != 0) 
  {
    universe_add(u, disp_offset_x, disp_offset_y, (Cell *)glider, 3, 3);
    glider_flag = 0;
    printf("universe add glider\r\n");
  }
  
  if(new_year != 0) 
  {
    universe_add(u, disp_offset_x, disp_offset_y, (Cell *)new_eayr_2020, 32, 16);
    new_year = 0;
  }
  
  if(my_osc_flag != 0) 
  {
    universe_add(u, disp_offset_x, disp_offset_y, (Cell *)my_osc2, 19, 19);
    my_osc_flag = 0;
  }
  
  if(pause == 0)
  {
    universe_next(u);
  }
  
  if(step != 0)
  {
    universe_next(u);
    step = 0;
  }
  
  //if((disp_offset_x > H) || (disp_offset_y > W)) return ;
  
  for (i = 0; i < DISPLAY_W; i++)
  {
    for (j = 0; j < DISPLAY_H; j++)
    {
      _x = disp_offset_x /*+ (DISPLAY_W / 2)*/ + (i /*- (DISPLAY_W / 4)*/) * disp_gain/64;
      _y = disp_offset_y /*+ (DISPLAY_H / 2)*/ + (j /*- (DISPLAY_H / 4)*/) * disp_gain/64;
      //if((_x > H) || (_y > W)) continue;
      
      ink = 0;
      if (disp_shadow_en)
      {
        ink = _calc_neighbours(u, _x, _y);
        //ink = _get_cell_neighbours(u, _x, _y);  // do not work!
      }
      
      if(disp_body_en)
      {
        if(_get_cell_state(u, _x, _y) == Cell_alive)
        {
          ink = 7;
        }
      }
      _draw_dot(i, j , ink);
    }
  }
}
