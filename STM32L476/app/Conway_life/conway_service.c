#include "conway.h"
#include "_conway.h"

void universe_clear(Cell u1[W][H])
{
  int i, j;

  for (i = 0; i < W; i++) {
    for (j = 0; j < H; j++) {
      u1[i][j].cell = 0;
    }
  }
}

void universe_fill(Cell u1[W][H], Cell cell)
{
  int i, j;

  for (i = 0; i < W; i++) {
    for (j = 0; j < H; j++) {
      u1[i][j] = cell;
    }
  }
}

void universe_rand(Cell g1[W][H])
{
  int i, j;

  for (i = 0; i < W; i++) {
    for (j = 0; j < H; j++) {
      g1[i][j]._cell.state = (Cell_state)(HAL_RNG_GetRandomNumber(&hrng)%2);
    }
  }
}


