#include "conway.h"
#include "_conway.h"

Cell_state _get_cell_state(Cell u[W][H], int x, int y)
{
  //if((x > H) || (y > W)) return Cell_dead;
  return u[x%W][y%H]._cell.state;
}

void _set_cell_state(Cell u[W][H], int x, int y, Cell_state state)
{
  u[x%W][y%H]._cell.state = state;
}
