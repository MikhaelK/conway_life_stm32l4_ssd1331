// Conway life internal header

#ifndef _CLIFE
#define _CLIFE

#include "conway.h"

#include "SSD1331.h" // display
#define DISPLAY_W RGB_OLED_WIDTH
#define DISPLAY_H RGB_OLED_HEIGHT

#define W CLIFE_W
#define H CLIFE_H


Cell_state _get_cell_state(Cell u[W][H], int x, int y);
void _set_cell_state(Cell u[W][H], int x, int y, Cell_state state);

void _set_cell_neighbours(Cell u[W][H], int x, int y, Cell_neighbours n);
Cell_neighbours _get_cell_neighbours(Cell g[W][H], int x, int y);
Cell_neighbours _calc_neighbours(Cell u[W][H], int x, int y);
//void _cpyMatrix(Cell g1[W][H], Cell g2[W][H]);

  
#endif /* _CLIFE */
