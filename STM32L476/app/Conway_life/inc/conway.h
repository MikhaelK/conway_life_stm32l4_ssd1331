// Conway life header
/*
/ The Game of Life, also known simply as Life, 
/ is a cellular automaton devised by the British 
/ mathematician John Horton Conway in 1970.
/ 
*/

#ifndef CLIFE
#define CLIFE
#include "board.h"
// universe_rand - board.h 
extern RNG_HandleTypeDef hrng;

// Universe dimensions 
#define CLIFE_W 96
#define CLIFE_H 64

/* Cell struct
  00000000
  ||||||||- state
  |||||||-- ng_0
  ||||||--- ng_1
  |||||---- ng_2
  ||||----- ng_3
  |||------ 
  ||------- 
  |-------- 
*/
typedef enum {
  Cell_dead = 0,
  Cell_alive = 1
}Cell_state; // 1 bit

typedef enum {
  Cell_zero_nb = 0,
  Cell_one_nb = 1,
  Cell_two_nb = 2,
  Cell_three_nb = 3,
  Cell_four_nb = 4,
  Cell_five_nb = 5,
  Cell_six_nb = 6,
  Cell_seven_nb = 7,
  Cell_ate_nb = 8
}Cell_neighbours; // 4 bit

#pragma pack(push, 1)
typedef struct {
  Cell_state state :1;
  Cell_neighbours n  :4;
  unsigned nc :3;
}Cell_bit;

typedef union {
  uint8_t   cell;
  Cell_bit  _cell;
}Cell;
#pragma pack(pop)

// Univerce object
#define OBJ_H   8
#define OBJ_W   8


void universe_add_nxn(Cell u[CLIFE_W][CLIFE_H], int x, int y, Cell obj[OBJ_W][OBJ_H]);
void universe_add(Cell u[CLIFE_W][CLIFE_H], int x, int y, Cell *obj, int w, int h);

extern volatile int universe_add_object0;
extern Cell object0[OBJ_W][OBJ_H];
extern volatile int universe_add_object1;
extern Cell glider[3][3];
extern volatile int glider_flag;
extern volatile int new_year;
extern Cell new_eayr_2020[32][16];
extern volatile int my_osc_flag;
extern Cell my_osc[13][6];
extern Cell my_osc2[19][19];
// Universe
//extern uint8_t universe[CLIFE_H][CLIFE_W];
extern volatile int universe_new_rand;
extern volatile int universe_new_clear;
// display features
extern volatile int disp_gain;
extern volatile int disp_color_table;
extern volatile int disp_body_en;
extern volatile int disp_shadow_en;
extern volatile int disp_offset_x; 
extern volatile int disp_offset_y;
extern volatile int pause, step;

void universe_fill(Cell u1[CLIFE_W][CLIFE_H], Cell cell);
void universe_clear(Cell u1[CLIFE_W][CLIFE_H]);
void universe_rand(Cell u1[CLIFE_W][CLIFE_H]);

void universe_next(Cell u[CLIFE_W][CLIFE_H]);

//void universe_draw(uint8_t u[CLIFE_W][CLIFE_H]);
void universe_draw_window(Cell u[CLIFE_W][CLIFE_H]);

#endif /* CLIFE */
